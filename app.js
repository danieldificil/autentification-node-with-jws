require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const app = express()

//Credentials
const dbUser = process.env.DB_USER
const dbPassword = process.env.DB_PASS

//Models
const User = require('./models/User')

//Config JSON response
app.use(express.json())

//Open Route - Public Route
app.get('/', (req, res) =>
{
    res.status(200).json({msg: "Hello wolrd"})
})

//Private Route
app.get('/user/:id', checkToken, async (req,res) =>
{
    const id = req.params.id

    //Check if user exists
    const user = await User.findById(id, '-password')

    if(!user)
    {
        return res.status(400).json({msg: "usuário não encontrado!"})
    }
    res
    .status(200)
    .json({ user })
})

//Register User
app.post('/auth/register', async(req,res) =>
{
    const {name, email, password, confirmpassword} = req.body

    //Validation
    if(!name)
    {
        return res.status(422).json({msg: 'O nome é obrigatório'})
    }
    if(!email)
    {
        return res.status(422).json({msg: 'O email é obrigatório'})
    }
    if(!password)
    {
        return res.status(422).json({msg: 'A senha é obrigatória'})
    }
    if(password !== confirmpassword)
    {
        return res.status(422).json({msg: 'As senhas não conferem'})
    }

    //Create user
    const user = new User
    ({
        name,
        email,
        password : passwordHash
    })

    try
    {
        await user.save()
        res
        .json
        ({
            msg: 'Usuário criado com sucesso!'
        })
    } 
        catch
        {
            res
            .status(500)
            .json
            ({
                msg: 'Aconteceu um erro no servidor, tente novamente mais tarde!'
            })
        }

    //Check if user exists
    const userExists = await User.findOne({ email : email})

    if(userExists)
    { 
        res.status(422).json({msg:"Por favor, utilize outro email"})
    }

    //Create password
    const salt = await bcrypt.genSalt(12)
    const passwordHash = await bcrypt.hash(password, salt)
})

//Login User
app.post("/auth/login", async (req,res)=>
{
    const {email, password} = req.body
    //Validation
    if(!email)
    {
        return res.status(422).json({msg: 'O email é obrigatório'})
    }
    if(!password)
    {
        return res.status(422).json({msg: 'A senha é obrigatória'})
    }
    //Check if user exists
    const user = await User.findOne({ email : email})

    if(!user)
    { 
        res.status(422).json({msg:"Usuário não encontrado"})
    }

    //Check if password match
    const checkPassword = await bcrypt.compare(password, user.password)

    if(!checkPassword)
    {
        return res.status(422).json({msg: 'Senha inválida'})
    }

    try
    {
        const secret = process.env.SECRET
        const token = jwt.sign
        (
            {
                id: user._id
            },
            secret
        )
        res.status(200).json({msg: 'Autentição realizada com sucesso!', token})
    }
        catch
        {
            res
            .status(500)
            .json
            ({
                msg: 'Aconteceu um erro no servidor, tente novamente mais tarde!'
            })
        }
})

//Validation token
function checkToken(req,res, next)
{
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(" ")[1]

    if(!token)
    {
        return res.status(401).json({msg: "Acesso negado!"})
    }
    try
    {
        const secret = process.env.SECRET
        jwt.verify(token, secret)

        next()
    } 
        catch(err)
        {
            res.status(400).json({msg : "Acesso negado!"})
        }
}

mongoose
.connect(`mongodb+srv://${dbUser}:${dbPassword}@cluster0.mqmlghf.mongodb.net/?retryWrites=true&w=majority`)
.then(() =>
{
    app.listen(3000)
    console.log('JÁ CHEGOU O DISCO VOADOR')
})
.catch((err) => console.log(err))
